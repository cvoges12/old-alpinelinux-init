# Alpine Chroot


### Installation

To create a base alpine chroot on any alpine system, run:

```sh
chmod +x *.sh
./alpine_init.sh
```
But this is assuming your current working directory is the directory of the
cloned repository.

When it is your first time in the chroot, run:

```sh
./chroot_init.sh
```


### Usage

Most people will go "into" the chroot via:

```sh
chroot /mnt/chroot sh
```

But you must remember that you can just as well run any program in the chroot
as:

```sh
chroot /mnt/chroot <program>
```

Because **chroots are not jails, containers, or sandboxes**. They simply change
what the system sees as the "root" point of the file system.
